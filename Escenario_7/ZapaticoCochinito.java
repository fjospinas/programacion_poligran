/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */

import java.util.Scanner;

public class ZapaticoCochinito {
	
	// Cara vez que un niño se extrae su posición en el array se convierte en null
	// Esta función cuenta cuántos se han extraido
	public static int CountNull(String [] Children, int k){
		int counter = 0;
		for(int i = 0; i < k; i++) {
			if(Children[i] == null) counter++;
		}
		
		return counter;
	}
	
	// Estrae el único niño no seleccionado de form aleatoria
	public static String ExtractLastChild(String [] Children, int k){
		String ret = null;
		for(int i = 0; i < k; i++) {
			if(Children[i] != null) {
				ret = Children[i];
			}
		}
		return ret;
	}

	public static void main(String[] args) {
		Scanner Lector = new Scanner(System.in);
		
		System.out.print("Ingresa la cantidad de ninos en el juego: ");
		int k = Lector.nextInt();
		
		String [] Children = new String[k];
 		
		// Lectura de los niños que participaran en el juego
		for(int i = 0; i < k; i++) {
			System.out.print("Ingrese el niño para la posición " + i + ": ");
			Children[i] = Lector.next();
		}
		
		// Imprime en pantalla los niños que perticipan
		System.out.println("Los niños que participarán en el juego son:");
		for(int i = 0; i < k; i++) {
			System.out.println("Nombre del niño: " + Children[i] + ", en la posición: " + i);
		}
		
		// Se genera un número aleatorio, se imprime en pantalla, se extrae al niño
		// EL niño que lava la loza es el último en ser extraido
		Boolean ContinueWhile = true;
		int azar = 0;
		String LastChild = null;
		while(ContinueWhile) {
			 azar = (int)(Math.random() * k);
			 if(Children[azar] != null){
				 System.out.println("El numero aleatorio al generado azar del niño que se excluye es : " + azar);
				 System.out.println("El niño que se excluye es : " + Children[azar] + "\n");
				 Children[azar] = null;
			 }
			 if(CountNull(Children, k) == k - 1) {
				 LastChild = ExtractLastChild(Children, k);
				 ContinueWhile = false;
			 }
		 }
		
		System.out.println("El niño que debe lavar la loza es: " + LastChild);		
		Lector.close();
	}

}