/*
 * Autores:
 * Alzate Palacio Oscar Andres
 * Pulgarin Londoño Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 * Francisco Javier Ospina Salazar
 * 
 * Este programa solicita la cantidad de datos que tendra el arreglo, 
 * luego llena el arreglo con numero aleatorios entre el 0-99,
 * Luego ordenar el arreglo en orden ascendente,
 * y halla la mediana del arreglo para mostrar en pantalla,
 *  para salir del programa se debe presionar la letra x
 */



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

public class MedianaArreglo {
	public static void main(String[] args) throws NumberFormatException, IOException {
		
		
		int cantidad;
		String datoIngresado;
		
		do {
			
			System.out.println("Ingrese tamaño del arreglo o x para salir: ");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			datoIngresado = br.readLine();
			if(!datoIngresado.equals("x")) {
				cantidad = Integer.parseInt(datoIngresado);
				float[] elementos = new float[cantidad];
				llenarArreglo(elementos, cantidad);
				ordenarArreglo(elementos);
				float mediana = buscarMediana(elementos);
				System.out.println("La mediana es: "+mediana);
			}
		}while(!datoIngresado.equals("x"));
		System.out.println("Cierre exitoso");
		System.exit(0);
	}
	
	public static void llenarArreglo(float[] arreglo, int cantidad){
		int i = 0;
		Random random = new Random();
		System.out.println("Generando numeros aleatorios para el arreglo.");
		String arregloOriginal = "Arreglo Original: ";
		while(i<cantidad) {
			float numero = random.nextInt(100);
			arregloOriginal += " "+numero;
			arreglo[i] = numero;
			i++;
		}
		System.out.println(arregloOriginal);
	}
	
	public static void ordenarArreglo(float[] arreglo){
		Arrays.sort(arreglo);
		String arregloOrdenado = "Arreglo Ordenado: ";
		int i = 0;
		
		while(i<arreglo.length) {
			arregloOrdenado += " "+arreglo[i];;
			i++;
		}
		
		System.out.println(arregloOrdenado);
	}
	
	public static float buscarMediana(float[] arreglo){
		float mediana;
		int mitad = arreglo.length / 2;
		if (arreglo.length % 2 == 0) {
		    mediana = (arreglo[mitad - 1] + arreglo[mitad]) / 2;
		} else {
		    mediana = arreglo[mitad];
		}
		return mediana;
	}
	
	
}

