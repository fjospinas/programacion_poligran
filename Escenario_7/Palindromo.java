/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */

import java.util.Scanner;

public class Palindromo {
	
	public static boolean es_palindromo(String S) {
		/*
		 * Si la longitud es par se comparan los elementos primero con último, segundo con penúltimo
		 * y así hasta llegar a los dos del medio.
		 * 
		 * Si la longitud es impar el procedimiento es el mismo y no se comnpara el elemento del medio
		 */
		int length = S.length();
		int index_comparison = length / 2;
		char SArray[] = S.toCharArray();

		
		for(int i=0; i < index_comparison; i++) {
			if(SArray[i] != SArray[length - 1 - i]) {
				return false;
			}
		}		
		return true;
	}

	public static void main(String[] args) {
		// Definición de scaner para captura de datos
		Scanner Lector = new Scanner(System.in);
				
		System.out.print("Por favor ingrese la cadena de texto S: ");
		String Input = Lector.nextLine();
		
		boolean palindromo = es_palindromo(Input);
		
		// Imprime mensaje con el resultado
		if(palindromo) {
			System.out.println("La cadena: " + Input + " es un palíndromo");
		}else {
			System.out.println("La cadena: " + Input + " No es un palíndromo");
		}
		
		Lector.close();

	}

}
