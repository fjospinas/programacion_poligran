/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;


public class CuantosDiferentes {
	
	public static void main(String[] args) { 
		Scanner Lector = new Scanner(System.in);
		System.out.print("Ingrese el número de elementos del array: ");
		
		int k = Lector.nextInt();
		
		int [] ArrayInt = new int[k];
		
		for(int i=0; i<k; i++) {
			System.out.print("Ingrese el número de ubicado en la posición " + i + ": ");
			ArrayInt[i] = Lector.nextInt();
		}
		
		System.out.println("El array de enteros original es:");
		System.out.println(Arrays.toString(ArrayInt));
		
		HashMap<Integer, Integer> map = new HashMap<>();
		for(int i=0; i<k; i++) {
			if(map.containsKey(ArrayInt[i])) {
				map.replace(ArrayInt[i], map.get(ArrayInt[i]) + 1);
			}else {
				map.put(ArrayInt[i], 1);
			}
		}		
		System.out.println("Se tienen " + map.size() + " elementos distintos");
		System.out.println("El HashMap con los conteos de cada elemento es: " + map);
		
		Lector.close();
	} 

}


