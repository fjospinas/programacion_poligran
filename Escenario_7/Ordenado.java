/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */

import java.util.Arrays;
import java.util.Scanner;

public class Ordenado {

	public static void main(String[] args) {
		Scanner Lector = new Scanner(System.in);
		System.out.print("Ingrese el número de elementos del array: ");
		
		int k = Lector.nextInt();
		
		int [] ArrayInt = new int[k];
		
		for(int i = 0; i < k; i++) {
			System.out.print("Ingrese el número de ubicado en la posición " + i + ": ");
			ArrayInt[i] = Lector.nextInt();
		}
		
		System.out.println("El array de enteros original es:");
		System.out.println(Arrays.toString(ArrayInt));
		
		Boolean IsSort = true;
		
		for(int i = 0; i < k - 1; i++) {
			if(ArrayInt[i] > ArrayInt[i+1]){
				IsSort = false;
				break;
			}
		}
		
		if(IsSort) {
			System.out.println("El array está ordenado de menor a mayor.");
		}else {
			System.out.println("El array NO está ordenado de menor a mayor.");
		}
		
		Lector.close();

	}

}
