/*
 * Autores:
 * Alzate Palacio Oscar Andres
 * Pulgarin Londoño Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 * Francisco Javier Ospina Salazar
 * 
 * Este programa recibe un numero y valida si es primo o no, para salir del programa
 * se debe presionar la letra x
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ContrasenaSegura {
	public static void main(String[] args) throws NumberFormatException, IOException {
		String contrasena;
		String datoIngresado;
		
		do {
		System.out.println("Ingrese contraseña o x para salir: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		datoIngresado = br.readLine();
		if(!datoIngresado.equals("x")) {
			contrasena = datoIngresado;
		    boolean resultado = validarSeguridad(contrasena);
		    if(resultado) {
		    	System.out.println("La contraseña es segura.");
		    } else {
		    	System.out.println("La contraseña no es segura.");
		    }
		}
	    
		}while(!datoIngresado.equals("x"));
		System.out.println("Cierre exitoso");
		System.exit(0);
		
	}
	
	public static boolean validarSeguridad(String contrasena){
		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.*])(?=\\S+$).{8,}";
		return contrasena.matches(pattern);
	}
	
}

