/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */

import java.util.Scanner;

public class BusquedaArreglo {
		

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		int cantidadEnteros;
		
		do {
			System.out.print("Digite la cantidad de enteros: ");
			cantidadEnteros = Integer.parseInt(entrada.nextLine());
			
			if (cantidadEnteros <= 0) {
				System.out.println();
				System.out.println("MENSAJE: Debe digitar un número entero.");
				System.out.println();
			}
		} while (cantidadEnteros <= 0);
		
		int[] x = new int[cantidadEnteros];
		
		System.out.println();
		
		for (int i = 0; i < x.length; i++) {
			System.out.print("Digite un número entero: ");
			x[i] = Integer.parseInt(entrada.nextLine());
			
			System.out.println();
		}
		
		System.out.println();
		
		System.out.print("Digite el valor a buscar: ");
		int n = Integer.parseInt(entrada.nextLine());
		
		System.out.println();
		
		int posicion = buscar(x, n);
		
		if (posicion != -1) {
			System.out.printf("El valor %d se encontró en la posición %d (empezando desde el indice 0).\n", n, posicion);
		} else {
			System.out.println("No se encontró el valor especificado.");
		}
		
		entrada.close();
	}
	
	public static int buscar(int[] x, int n) {for(int i = 0; i < x.length; ++i) {
			if (x[i] == n) {
				return i;
			}
		}
		
		return -1;
	}

}
