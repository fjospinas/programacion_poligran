/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */

import java.util.Scanner;
import java.util.Arrays;

public class Anagramas {
	
	public static boolean are_anagram(String S, String T) {
		// Ordena los dos strings
		char SSort[] = S.toCharArray();
		char TSort[] = T.toCharArray();
		
		Arrays.sort(SSort);
		Arrays.sort(TSort);
		
		// Si un solo caracter es distinto retorna false
		int i = 0;
		while(i < SSort.length) {
			if(SSort[i] != TSort[i]) {
				return false;
			}
			i++;
		}		
		
		// Si después de hacer un barrido todos los carácteres son iguales retorna True
		return true;
	}

	public static void main(String[] args) {
		// Definición de scaner para captura de datos
		Scanner Lector = new Scanner(System.in);
		
		System.out.print("Por favor ingrese la cadena de texto S: ");
		String S = Lector.nextLine();
		System.out.print("Por favor ingrese la cadena de texto T: ");
		String T = Lector.nextLine();
		
		/*
		 * Si las dos cadenas tienen el mismo tamaño entra a comparar caracter por caracter
		 * sino automaticamente NO son anagramas
		 */
		boolean equals;
		if(S.length() == T.length()) {
			equals = are_anagram(S, T);
		}else {
			equals = false;
		}
		
		// Imprime mensaje con el resultado
		if(equals) {
			System.out.println("La cadena T: " + T + " es un anagrama de la cadena S: " + S);
		}else {
			System.out.println("La cadena T: " + T + " NO es un anagrama de la cadena S: " + S);
		}
		
		Lector.close();
	}

}
