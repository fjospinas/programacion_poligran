import java.util.Scanner;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar, cód: 1711982465
 * Oscar Andrés Alzate Palacio, cód: 100224270
 * Santiago Pulgarin Londono, cód: 2021024921
 * Eymi Jaraba Higuita, cód:
 * Diana Marcela Fernandez Diaz, cód:
 */

public class temperatura {

	// Función main que lee los datos e imprime en pantalla
	public static void main(String[] args) {
		System.out.print("ingrese la cantidad de grados centigrados a convertir: ");
		
		// Lectura de datos
		Scanner Lector = new Scanner(System.in);
		double C =Lector.nextDouble();
		
		// Impresión de conversiones
		System.out.println(C + " grados centigrados son equivalente a " + rankine(C) + " grados rankine");
		System.out.println(C + " grados centigrados son equicalente a " + fahrenheit(C) + " grados fahrenheit");
		System.out.println(C + " grados centigrados son equivalente a " + kelvin(C)+" grados kelvin");
		
		Lector.close();
	}
		
	// Conversión de centigrados a rankine
	static double rankine (double c) {
		double r = 9/5.0 * c + 491.67;
		return r;
	}
	
	// Conversión de centigrados a frahrenheit
	static double fahrenheit (double c) {
	    double f = 9/5.0 + c + 32;
	    return f;
	}
	
	// Conversión de centigrados a kelvin
	static double kelvin (double c) {
		double k = c + 273.15;
		return k;
	}

}
