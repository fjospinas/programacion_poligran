import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/*
 * Integrantes Grupo:
 * 
 * Oscar Andrés Alzate Palacio, cód: 100224270
 * Francisco Javier Ospina Salazar, cód: 1711982465
 * Santiago Pulgarin Londono, cód: 2021024921
 * Eymi Jaraba Higuita, cód:
 * Diana Marcela Fernandez Diaz, cód:
 */


// Clase que transforma de forma iterativa metros a otras unidades
public class metros_a_otras_unidades {

	// Función main
	public static void main(String[] args) throws NumberFormatException, IOException {
		float cantidadMetros;
		String datoIngresado;
		
		do {
			System.out.println("Ingrese valor en metros o presione x para salir: ");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			datoIngresado = br.readLine();
			if(!datoIngresado.equals("x")) {
				cantidadMetros = Float.parseFloat(datoIngresado);
				convertirCentimetros(cantidadMetros);
				convertirMilimetros(cantidadMetros);
				convertirPies(cantidadMetros);
				convertirPulgadas(cantidadMetros);
				convertirYardas(cantidadMetros);
				convertirAnosLuz(cantidadMetros);
				convertirAngstroms(cantidadMetros);
			}
		} while(!datoIngresado.equals("x"));
		System.out.println("Cierre exitoso");
		System.exit(0);
	}
	
	// Conversión a centimetros
	private static void convertirCentimetros(float numero) {
		float valor = numero * 100;
		System.out.println("Valor en centimetros: "+valor);
	}
	
	//Conversión a milimetros
	private static void convertirMilimetros(float numero) {
		float valor = numero * 1000;
		System.out.println("Valor en milimetros: "+valor);
	}

	//Conversión a pies
	private static void convertirPies(float numero) {
		float valor = (float) (numero * 3.28084);
		System.out.println("Valor en Pies: "+valor);
	}
	
	//Conversión a pulgadas
	private static void convertirPulgadas(float numero) {
		float valor = (float) (numero * 39.37008);
		System.out.println("Valor en Pulgadas: "+valor);
	}
	
	//Conversión a yardas
	private static void convertirYardas(float numero) {
		float valor = (float) (numero * 1.0936133333333);
		System.out.println("Valor en Yardas: "+valor);
	}
	
	//Conversión a años luz
	private static void convertirAnosLuz(float numero) {
		float valor = (float) (numero * 1.057e-16);
		System.out.println("Valor en Años Luz: "+valor);
	}
	
	//Conversión a angstroms
	private static void convertirAngstroms(float numero) {
		float valor = (float) (numero * 1e+10);
		System.out.println("Valor en Angstroms: "+valor);
	}
}
	
