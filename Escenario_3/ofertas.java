import java.util.Scanner;
import java.util.Arrays;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar, cód: 1711982465
 * Oscar Andrés Alzate Palacio, cód: 100224270
 * Santiago Pulgarin Londono, cód: 2021024921
 * Eymi Jaraba Higuita, cód:
 * Diana Marcela Fernandez Diaz, cód:
 */


public class ofertas {

	public static void main(String[] args) {
		
		// Scaner para leer los precios de las camisetas
		Scanner lector = new Scanner(System.in);
		
		double A, B, C ,D;
		
		// Lectura de datos
		System.out.print("Ingresar el valor de A: ");
		A = lector.nextDouble() ;
		System.out.print("Ingresar el valor de B: ");
		B = lector.nextDouble() ;
		System.out.print("Ingresar el valor de C: ");
		C = lector.nextDouble() ;
		System.out.print("Ingresar el valor de D: ");
		D = lector.nextDouble() ;
		
		// Calculo de costos
		double Valor1 = 3*A - ((D * 3*A) / 100);
		double Valor2 = 2*B;
		double Valor3 = 3*C;
		
		// Impresón del valor mínimo
		double[] valores = new double[]{Valor1, Valor2, Valor3}; 
		
		Arrays.sort(valores);
		System.out.println("El menor valor que puede pagar Bélen es: " + valores[0]);
		
		// Cerrar scanner
		lector.close();
		

	}

}
