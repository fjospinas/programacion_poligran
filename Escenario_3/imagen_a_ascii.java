import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.nio.file.Path;
import java.nio.file.Paths;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar, cód: 1711982465
 * Oscar Andrés Alzate Palacio, cód: 100224270
 * Santiago Pulgarin Londono, cód: 2021024921
 * Eymi Jaraba Higuita, cód:
 * Diana Marcela Fernandez Diaz, cód:
 */

public class imagen_a_ascii {
	
	// Obtiene una matriz en dos simensiones con los varoles en escala de grises de los pixeles
	public static int[][] valores_escala_grises(File file) throws IOException {
		
		// Lectura del archivo y sus dimensiones
		BufferedImage img = ImageIO.read(file);
		int ancho = img.getWidth();
		int alto = img.getHeight();
		
		// Matriz de pixeles de la imagen
		int[][] vector_pixeles = new int[ancho][alto];
		Raster raster = img.getData();
		 
		// Iteración sobre el Raster para llenar la matriz de pixeles
		for (int i = 0; i < ancho; i++) {
		    for (int j = 0; j < alto; j++) {
		    	vector_pixeles[i][j] = raster.getSample(i, j, 0);
		    }
		}
		
		// Retorno de la matriz de pixeles
		return vector_pixeles;
	}
	
	public static void pintar_ascii(int[][] vector_pixeles) throws IOException {	
		
		/* 
		 * Caracteres ascii que vamos a usar 
		 * Se una escala de 70 tonos de gris
		 * Estos caracteres fueron extraidos de: http://paulbourke.net/dataformats/asciiart/
		 */
		String vector_ascii_chars = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ";
		int indice_val = 0;
		
		// Iterador para transformar cara tono gris en un caracter ascii y lo imprime		
		for(int i=0; i < vector_pixeles[0].length; i++){
			for(int j=0; j < vector_pixeles.length ; j++){
				
				// El valor de 3.65 se obtiene de: 256 / 70 = 3.65
				// donde 256 es la escala de grises y 70 son los caracteres Ascii a usar
				
				indice_val = (int) ((vector_pixeles[j][i] / 3.65));
				if(indice_val == 70) {
					indice_val = 69;
				}
				System.out.print(vector_ascii_chars.charAt(indice_val));				
			}
			System.out.println("");
		}
	}

	public static void main(String[] args) throws IOException{
		
		// Se define el nombre del archivo a leer
		String nombre_archivo = "Homero.jpg";		
		
		// Concatena la ruta absoluta para leer el archivo
		Path ruta_actual = Paths.get(System.getProperty("user.dir"));
		Path ruta_archivo = Paths.get(ruta_actual.toString(), "imagenes_prueba", nombre_archivo);
		System.out.println(ruta_archivo.toString());		
		
		// Ruta del archivo en disco a escala de grises
		File file = new File(ruta_archivo.toString());
		
		// Matriz con los valores en escala de grises
		int[][] imgArr = valores_escala_grises(file);
        
		// Pintar en modo Ascii la imagen
		pintar_ascii(imgArr);
	}

}
