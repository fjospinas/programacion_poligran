import java.util.Scanner;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar, cód: 1711982465
 * Oscar Andrés Alzate Palacio, cód: 100224270
 * Santiago Pulgarin Londono, cód: 2021024921
 * Eymi Jaraba Higuita, cód:
 * Diana Marcela Fernandez Diaz, cód:
 */

public class hora_a_segundos {
	
	/*
	 * Método utilitario que hace la conversión de horas minutos y segundos 
	 * a segundos transcurridos desde la media noche
	 */
	static int convert_hora(int horas, int minutos, int segundos) {
		return segundos + (minutos * 60) + (horas *3600);
	}

	// Función main para invocar al método previamente creado
	public static void main(String[] args) {
		
		// Scaner para leer grados centigrados
		Scanner lector = new Scanner(System.in);
		
		// Variables 
		int horas, minutos, segundos, segundos_transcurridos;
		
		// Lectura de hora actual
		System.out.print("Ingresar la hora (H:M:S) actual.\nHora:");
		horas = lector.nextInt() ;
		
		System.out.print("Minuto:");
		minutos = lector.nextInt() ;
		
		System.out.print("Segundos:");
		segundos = lector.nextInt();
		
		// Calculo de segundos transcurridos
		 segundos_transcurridos = convert_hora(horas, minutos, segundos);
		
		// Impresión en pantalla de segundos transcurridos
		System.out.println("Los segudos transcurridos desde la medianoche son: " + segundos_transcurridos);
		
		lector.close();
	}

}
