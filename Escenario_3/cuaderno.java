import java.util.Scanner;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar, cód: 1711982465
 * Oscar Andrés Alzate Palacio, cód: 100224270
 * Santiago Pulgarin Londono, cód: 2021024921
 * Eymi Jaraba Higuita, cód:
 * Diana Marcela Fernandez Diaz, cód:
 */

public class cuaderno {

	public static void main(String[] args) {
		
		// Definición de scaner para captura de datos
		Scanner Lector = new Scanner(System.in);
		
		// Lectura de variables
		System.out.print("Por favor ingrese la media de Alto de la página: ");
		double Alto = Lector.nextDouble();
		
		System.out.print("seguido la medida Ancho de la pagina: ");
		double Ancho = Lector.nextDouble();
		
		// Cálculo de cuadros
		double CuadrosAlto, CuadrosAncho;
		CuadrosAlto = (Alto - 2) / 0.5;
		CuadrosAncho = (Ancho - 1) / 0.5;
		
		// Impresión en pantalla
		System.out.println("La cantidad de cuadros en la hoja es de: " + (CuadrosAlto * CuadrosAncho));
		
		// Cerrar lector
		Lector.close();
	}
	
}
