import java.util.Scanner;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */


public class NumeroParImpar
{
	private static Scanner teclado;

	public static void main( String[] args )
	{
		int numero;
		teclado = new Scanner( System.in );

		System.out.printf( "Introduzca un número entero: " );
		numero = teclado.nextInt();

		if ( numero % 2 == 0 ){
			System.out.printf("El número " + numero + " es par.");
		}
		else{
			System.out.printf("El número " + numero + " es par.");
		}
		teclado.close();
	}
}