import java.util.Scanner;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */

public class FronteraRectangulo {

    public static void main(String[] args) {
    	
        int limiteXRectangulo = iniciarPuntoARectangulo();
        int limiteYRectangulo = iniciarPuntoBRectangulo();
        int miUbicacionX = 0;
        int miUbicacionY = 0;
        String opcion = "";
        do{
            System.out.println ("Por favor ingrese una opcion: ");
            System.out.println ("1 - ingresar mi posicion ");
            System.out.println ("0 - Salir ");
            Scanner entradaEscaner = new Scanner (System.in); 
            opcion = entradaEscaner.nextLine (); 
            if(opcion.equals("1")){
                miUbicacionX = iniciarMiPosicionX(limiteXRectangulo);
                miUbicacionY = iniciarMiPosicionY(limiteYRectangulo);
                
                int diferenciaXInicial  = Math.abs(0 - (miUbicacionX));
                int diferenciaXFinal    = Math.abs(limiteXRectangulo - (miUbicacionX));
                int diferenciaYAbajo    = Math.abs(0 - (miUbicacionY));
                int diferenciaYArriba   = Math.abs(limiteYRectangulo - (miUbicacionY));
                
                if(miUbicacionX==miUbicacionY){
                    if(miUbicacionX<(limiteXRectangulo/2)){
                        System.err.println("La frontera mas cercana son las coordenadas: (0"+","+miUbicacionY+")");
                    } else if(miUbicacionX>(limiteXRectangulo/2)){
                        System.err.println("La frontera mas cercana son las coordenadas: ("+miUbicacionX+","+limiteYRectangulo+")");
                    } else {
                        System.err.println("La frontera mas cercana son las coordenadas: (0,"+miUbicacionY+")");
                    }
                } else {
                
                    if(diferenciaXFinal == diferenciaXInicial && diferenciaYAbajo == diferenciaYArriba){
                        System.err.println("La frontera mas cercana son las coordenadas: (0,"+","+miUbicacionY+")");
                    }
                    if(diferenciaXFinal == diferenciaXInicial && diferenciaYAbajo < diferenciaYArriba){
                        System.err.println("La frontera mas cercana son las coordenadas: (0,"+miUbicacionY+")");
                    }
                    if(diferenciaXFinal == diferenciaXInicial && diferenciaYAbajo > diferenciaYArriba){
                        System.err.println("La frontera mas cercana son las coordenadas: (0"+miUbicacionY+")");
                    }

                    if(diferenciaXFinal < diferenciaXInicial && diferenciaYAbajo == diferenciaYArriba){
                        System.err.println("La frontera mas cercana son las coordenadas: ("+miUbicacionX+",0)");
                    }
                    if(diferenciaXFinal > diferenciaXInicial && diferenciaYAbajo == diferenciaYArriba){
                        System.err.println("La frontera mas cercana son las coordenadas: (0"+miUbicacionX+"0)");
                    }

                    if(diferenciaXInicial<diferenciaXFinal && diferenciaXInicial<diferenciaYAbajo && diferenciaXInicial < diferenciaYArriba){
                        System.err.println("La frontera mas cercana son las coordenadas: (0"+","+miUbicacionY+")");
                    }
                    if(diferenciaXFinal<diferenciaXInicial && diferenciaXFinal<diferenciaYAbajo && diferenciaXFinal < diferenciaYArriba){
                        System.err.println("La frontera mas cercana son las coordenadas: ("+limiteXRectangulo+","+miUbicacionY+")");
                    }

                    if(diferenciaYAbajo<diferenciaXInicial && diferenciaYAbajo<diferenciaXFinal && diferenciaYAbajo < diferenciaYArriba){
                        System.err.println("La frontera mas cercana son las coordenadas: ("+miUbicacionX+",0)");
                    }

                    if(diferenciaYArriba<diferenciaXInicial && diferenciaYArriba<diferenciaXFinal && diferenciaYArriba < diferenciaYAbajo){
                        System.err.println("La frontera mas cercana son las coordenadas: ("+miUbicacionX+","+limiteYRectangulo+")");
                    }
                }
                
            }
            entradaEscaner.close();
        } while (!opcion.equals("0"));               
    
    }
    
    public static int iniciarPuntoARectangulo(){
        System.out.println ("Ingrese posicion a del punto superior del rectangulo: ");
        String entradaA = "";
        Scanner entradaEscaner = new Scanner (System.in); 
        entradaA = entradaEscaner.nextLine (); 
        entradaEscaner.close();
        return Integer.parseInt(entradaA);
    }
    
    public static int iniciarPuntoBRectangulo(){

        Scanner entradaEscaner = new Scanner (System.in); 
        System.out.println ("Ingrese posicion b del punto superior del rectangulo: ");
        String entradaB = "";
        entradaB = entradaEscaner.nextLine(); 
        entradaEscaner.close();
        return Integer.parseInt(entradaB);
    }
    
    public static int iniciarMiPosicionX(int puntoXRectangulo){

        Scanner entradaEscaner = new Scanner (System.in); 
        int miPosicionX = 0;
        do{
            System.out.println ("Ingrese su posicion en x: ");
            String posicionX = "";
            posicionX = entradaEscaner.nextLine(); 
            miPosicionX = Integer.parseInt(posicionX);
            if(miPosicionX > puntoXRectangulo){
                System.err.println("Su posicion en x es mayor a la frontera del rentangulo que es "+puntoXRectangulo);
            }
            
        } while(miPosicionX>puntoXRectangulo);
        entradaEscaner.close();
        return miPosicionX;
    }
    
    public static int iniciarMiPosicionY(int puntoYRectangulo){

        Scanner entradaEscaner = new Scanner (System.in); 
        int miPosicionY = 0;
        do{
            System.out.println ("Ingrese su posicion en y: ");
            String posicionY = "";
            posicionY = entradaEscaner.nextLine(); 
            miPosicionY = Integer.parseInt(posicionY);
            if(miPosicionY > puntoYRectangulo){
                System.err.println("Su posicion en y es mayor a la frontera del rentangulo que es "+puntoYRectangulo);
            }
            
        } while(miPosicionY>puntoYRectangulo);
        entradaEscaner.close();
        return miPosicionY;
    }    
    
}
