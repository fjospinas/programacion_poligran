import java.util.Scanner;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */

public class conjetura_goldbach {
	
	 public static Boolean esPrimo (int n) { 
	        int i=2;        
	        while (n%i!=0) i++;                    
	        if (i==n) return true;
	        else return false;
	    }


	public static void main(String[] args) {
		// Definición de scaner para captura de datos
		Scanner Lector = new Scanner(System.in);
		
		// Captura de número que se va a descomponer
		System.out.print("Por favor ingrese el númmero natural par mayor a dos: ");
		int numeroPar = Lector.nextInt();
		
		// Bucle que controla la busqueda del primer número
		for(int i = 2; i < numeroPar; i++) {
			
			// Bucle que controla la busqueda del segundo número
			for(int j = 2; j < numeroPar; j++) {
				
				// Validación de si ambos número son primos
				if(esPrimo(i) && esPrimo(j)) {
					
					// Se rompe la búsqueda si ambos números suman el valor de numeroPar
					if(i + j == numeroPar) {
						System.out.println("Los numero primos cuya suma es " + numeroPar + " son: " + i + " y " + j);
						System.out.println(numeroPar + " = " + i + " + " + j);
						j = numeroPar;
						i = numeroPar;
					}
				}
				
			}
			
		}
		Lector.close();
		
	}
}
