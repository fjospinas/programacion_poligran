/*
 * Autores:
 * Alzate Palacio Oscar Andres
 * Pulgarin Londoño Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 * Francisco Javier Ospina Salazar
 * 
 * Este programa recibe un numero y valida si es primo o no, para salir del programa
 * se debe presionar la letra x
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NumeroPrimo {
	public static void main(String[] args) throws NumberFormatException, IOException {
		int numero;
		String datoIngresado;
		
		do {
		
			System.out.println("Ingrese un numero o x para salir: ");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			datoIngresado = br.readLine();
			if(!datoIngresado.equals("x")) {
				numero = Integer.parseInt(datoIngresado);
				boolean esPrimo = esPrimo(numero);
				if(esPrimo) {
					System.out.println("El numero ingresado es primo.");
				} else {
					System.out.println("El numero ingresado No es primo.");
				}
			}
		}while(!datoIngresado.equals("x"));
		System.out.println("Cierre exitoso");
		System.exit(0);
	}
	
	public static boolean esPrimo(int numero){
		  int contador = 2;
		  boolean primo=true;
		  while ((primo) && (contador!=numero)){
		    if (numero % contador == 0)
		      primo = false;
		    contador++;
		  }
		  return primo;
		}
}


