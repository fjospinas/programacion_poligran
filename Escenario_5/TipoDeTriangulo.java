import java.util.Scanner;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */


public class TipoDeTriangulo {
	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);
		double l1,l2,l3;
		System.out.print("Introduzca el Valor Numérico del 1 Lado: ");
		l1 = lector.nextDouble();
		System.out.print("Introduzca el Valor Numérico del 2 Lado: ");
		l2 = lector.nextDouble();
		System.out.print("Introduzca el Valor Numérico del 3 Lado: ");
		l3 = lector.nextDouble();
		if (l1==l2 && l2==l3)
			System.out.println("\nEl Triángulo es Equilatero\n");
		else
		{
			if (l1==l2 || l1==l3 || l2==l3)
				System.out.println("\nEl Triángulo es Isoceles");
			else
			{
				if (l1!=l2 || l1!=l3 || l3!=l2)
					System.out.println("\nEl Triángulo es Escaleno");
			}
		}
		lector.close();
	}
}