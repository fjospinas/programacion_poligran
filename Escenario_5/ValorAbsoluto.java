/*
 * Autores:
 * Alzate Palacio Oscar Andres
 * Pulgarin Londoño Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 * Francisco Javier Ospina Salazar
 * 
 * Este programa recibe un numero y retorna su valor absoluto, para salir del programa
 * se debe presionar la letra x
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ValorAbsoluto {
	public static void main(String[] args) throws NumberFormatException, IOException {
		float numero;
		float valorAbsoluto;
		String datoIngresado;
		
		do {
			
			System.out.println("Ingrese valor para hallar su valor absoluto o x para salir: ");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			datoIngresado = br.readLine();
			if(!datoIngresado.equals("x")) {
				numero = Float.parseFloat(datoIngresado);
				valorAbsoluto = hallarValorAbsoluto(numero);
				System.out.println("El valor absoluto es: "+valorAbsoluto);
			}
		
		} while(!datoIngresado.equals("x"));
		System.out.println("Cierre exitoso");
		System.exit(0);
		
	}
	
	private static float hallarValorAbsoluto(float numero) {
		float valorAbsoluto = Math.abs(numero);
		return valorAbsoluto;
	}
}

