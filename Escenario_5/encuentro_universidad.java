import java.util.Scanner;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */

public class encuentro_universidad {
	
	// Calcula el mínimo común múltiplo de tres números
	public static int Mcm(int n1, int n2, int n3) {
		
		int i = 2;
		int mcm = 1;
		Boolean continuar = true; 
		
		while(continuar){
			
			if (n1 % i == 0 || n2 % i == 0 || n3 % i == 0) {
				mcm = mcm * i;
				if (n1 % i == 0) n1 = n1 / i;
				if (n2 % i == 0) n2 = n2 / i;
				if (n3 % i == 0) n3 = n3 / i;
			}else{
				i++;
			}
			
			if(n1 == 1 && n2 == 1 && n3 == 1) {
				continuar = false;
			}
		}
		return mcm;
	}

	public static void main(String[] args) {
		
		// Definición de scaner para captura de datos
		Scanner Lector = new Scanner(System.in);
				
		// Captura de datos
		System.out.print("Por favor ingrese el número de dias que tarda Ana en ir a la Universidad: ");
		int A = Lector.nextInt();
		System.out.print("Por favor ingrese el número de dias que tarda Bernardo en ir a la Universidad: ");
		int B = Lector.nextInt();
		System.out.print("Por favor ingrese el número de dias que tarda Carlos en ir a la Universidad: ");
		int C = Lector.nextInt();
		
		int mcm = Mcm(A, B, C);
		System.out.println("Ana, Bernardo y Carlos coincidiránm nuevamente en la universidad en " + mcm + " días");
		
		Lector.close();
	}

}
