import java.util.Scanner;

/*
 * Integrantes Grupo:
 * 
 * Francisco Javier Ospina Salazar
 * Alzate Palacio Oscar Andrés
 * Pulgarin Londono Santiago
 * Jaraba Higuita Eymi
 * Diana Marcela Fernandez Diaz
 */

public class JuegoDeDardos {
	public static void main(String args[]) {
		double x=0,y=0,r=0,x2=0,y2=0,r2=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Este programa me bota el puntaje obtenido al lanzar un dardo a un tablero");
		System.out.println("Ingrese la coordenada X");
		x=sc.nextDouble();
		System.out.println("Ingrese la coordenada Y");
		y=sc.nextDouble();
		x2=Math.pow(x, 2);
		y2=Math.pow(y, 2);
		r2=x2+y2;
		r=Math.sqrt(r2);

		if(r<=1) {
			System.out.println("El dardo dio en el aro rojo");
			System.out.println("Su puntaje es de 15");
		}
		else {
			if(r<=2){
				System.out.println("El dardo dio en el aro naranja");
				System.out.println("Su puntaje es de 9");
			}
			else {
				if(r<=3) {
					System.out.println("El dardo dio en el aro amarillo");
					System.out.println("Su puntaje es de 5");
				}
				else {
					if(r<=4) {
						System.out.println("El dardo dio en el aro verde");
						System.out.println("Su puntaje es de 2");
					}
					else {
						if(r<=5) {
							System.out.println("El dardo dio en el aro azul");
							System.out.println("Su puntaje es 1");
						}
						else {
							System.out.println("El dardo no dio dentro del tablero");
							System.out.println("Su puntaje es de 0");
						}
					}
				}
			}
		}
		sc.close();
	}
}

